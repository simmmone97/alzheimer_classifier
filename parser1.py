import argparse


def parse():
    # Configure parser
    parser = argparse.ArgumentParser(
            description='Classify Alzheimer\'s presence and its severity'
        )
    parser.add_argument('-n', '--cnn', type=str, help='cnn of choice between VGG_16 or GOOGLE_NET or None to load a csv file previously stored in Output Products')
    parser.add_argument('-c', '--classifier', type=str, help='classifier of choice between PLS-DA or LDA or FFNET')
    # Parse & check options
    opts = parser.parse_args()

    if not((opts.cnn == "VGG_16") or (opts.cnn == "GOOGLE_NET") or (opts.cnn == "None")):
        print("CNN type non defined or not understood. Resorting to default value VGG_16")
        opts.cnn = "VGG_16"

    if not((opts.classifier == "PLS-DA") or (opts.classifier == "LDA") or (opts.classifier == "FFNET")):
        print("Classifier type not defined or not understood. Resorting to default value PLS-DA")
        opts.classifier = "PLS-DA"

    return opts

