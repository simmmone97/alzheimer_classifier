# -*- coding: utf-8 -*-
"""
Created on Tue Jan 18 10:40:06 2022

@author: simone
"""

# from keras.applications.vgg16 import preprocess_input
# import os
# import PIL
# import pandas as pd

import matplotlib.pyplot as plt
import numpy as np
import tensorflow as tf
import pathlib
from keras.applications.vgg16 import VGG16
from keras import layers, models


# definisco i dataset di train, validation e test
train_dir = pathlib.Path("Alzheimer_Dataset/train")
test_dir = pathlib.Path("Alzheimer_Dataset/test")

BATCH_SIZE = 32
IMG_SIZE = (208, 176)
METRICS = [
      # tf.keras.metrics.BinaryAccuracy(name='accuracy'),
      tf.keras.metrics.CategoricalAccuracy(name="categorical_accuracy"),
      tf.keras.metrics.Precision(name='precision'),
      tf.keras.metrics.Recall(name='recall'),
      tf.keras.metrics.AUC(name='auc'),
      tf.keras.metrics.SpecificityAtSensitivity(0.5, num_thresholds=300,
                                             name=None, dtype=None),
      # tfa.metrics.F1Score(num_classes=4)
]

train_dataset = tf.keras.utils.image_dataset_from_directory(train_dir,
                                                            label_mode="categorical",
                                                            validation_split=0.2,
                                                            subset="training",
                                                            seed=123,
                                                            batch_size=BATCH_SIZE,
                                                            image_size=IMG_SIZE)
validation_dataset = tf.keras.utils.image_dataset_from_directory(train_dir,
                                                                 label_mode="categorical",
                                                                 validation_split=0.2,
                                                                 subset="validation",
                                                                 seed=123,
                                                                 batch_size=BATCH_SIZE,
                                                                 image_size=IMG_SIZE)
test_dataset = tf.keras.utils.image_dataset_from_directory(test_dir,
                                                           label_mode="categorical",
                                                           shuffle=True,
                                                           batch_size=BATCH_SIZE,
                                                           image_size=IMG_SIZE)

train_dataset_tot = tf.keras.utils.image_dataset_from_directory(train_dir,
                                                                batch_size=1,
                                                                image_size=IMG_SIZE)
class_names = train_dataset.class_names
print(class_names)

# prealloco la memoria per velocizare le epoche
# AUTOTUNE = tf.data.AUTOTUNE

# train_dataset = train_dataset.prefetch(buffer_size=AUTOTUNE)
# validation_dataset = validation_dataset.prefetch(buffer_size=AUTOTUNE)
# test_dataset = test_dataset.prefetch(buffer_size=AUTOTUNE)

# mobilenet V2 necessita di valori tra[-1,1] e non [0,255], li riscaliamo con rescale (funziona?)
rescale = tf.keras.layers.Rescaling(1./127.5, offset=-1)

# importo mobilenet v2 co default input shape (224,224) invece che (208,176)
IMG_SHAPE = IMG_SIZE + (3,)
base_model = VGG16(weights="imagenet", include_top=False, input_shape= IMG_SHAPE)
base_model.trainable = False

# controllo la grandezza dei batch
image_batch, label_batch = next(iter(train_dataset_tot))
feature_batch = base_model(image_batch)
print(feature_batch.shape)
# image_feature_test = np.copy(feature_batch[1])
# image_feature_test = np.array(image_feature_test)
# img_feat_test_no0 =  [i for i in image_feature_test if i != 0]



# riassunto modello
base_model.summary()

# immagine dello spazio delle feature all'ultimo layer VGG16


flatten_layer = layers.Flatten()
dense_layer_1 = layers.Dense(50, activation='relu')
dense_layer_2 = layers.Dense(20, activation='relu')
prediction_layer = layers.Dense(4, activation='softmax')



# creiamo il modello (senza data augmentione, pre processing e dropout)
inputs = tf.keras.Input(shape=(208, 176, 3))
# x = inputs
# x = base_model(x, training=False)
# x = FNN_layer(x)
# #x = FNN_layer2(x)
# outputs = prediction_layer(x)
# model = tf.keras.Model(inputs, outputs)

model = tf.keras.Sequential()
model.add(inputs)
model.add(base_model)
model.add(flatten_layer)
# model.add(dense_layer_1)
# model.add(dense_layer_2)
# model.add(prediction_layer)

# studio delle features, le carico in una cartella
features_tot = np.zeros([len(train_dataset_tot), model.output_shape[1]])

a = "OutputProducts/features_VGG16.npy"

for i in range(10):
    bottleneck_feature_example = model.predict(next(iter(train_dataset_tot))[0])
    features_tot[i] = bottleneck_feature_example[0]


np.save(a, features_tot)

#compile model
base_learning_rate = 0.0001
model.compile(optimizer=tf.keras.optimizers.Adam(learning_rate=base_learning_rate),
              loss=tf.keras.losses.CategoricalCrossentropy(),
              metrics=METRICS)
#model summary
model.summary()

#pesi e biases della rete sono i parametri addestrabili
len(model.trainable_variables)

##train del modello
initial_epochs = 10

##loss e accuratezza prima del training
#loss0, accuracy0 = model.evaluate(validation_dataset)
plt.figure(figsize=(8, 8))
plt.subplot(2, 1, 1)
plt.legend(loc='lower right')
plt.ylabel('Accuracy')
plt.show()

#allenamento per 10 epoche
history = model.fit(train_dataset,
                    epochs=initial_epochs,
                    validation_data=validation_dataset)

# accuracy e loss plot
acc = history.history['accuracy']
val_acc = history.history['val_accuracy']

loss = history.history['loss']
val_loss = history.history['val_loss']

plt.figure(figsize=(8, 8))
plt.subplot(2, 1, 1)
plt.plot(acc, label='Training Accuracy')
plt.plot(val_acc, label='Validation Accuracy')
plt.legend(loc='lower right')
plt.ylabel('Accuracy')
plt.ylim([min(plt.ylim()),1])
plt.title('Training and Validation Accuracy')

plt.subplot(2, 1, 2)
plt.plot(loss, label='Training Loss')
plt.plot(val_loss, label='Validation Loss')
plt.legend(loc='upper right')
plt.ylabel('Cross Entropy')
plt.ylim([0, 1.0])
plt.title('Training and Validation Loss')
plt.xlabel('epoch')
plt.show()

#test accurcy
loss, accuracy = model.evaluate(test_dataset)
print('Test accuracy :', accuracy)