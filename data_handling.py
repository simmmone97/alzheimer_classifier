import numpy as np
from sklearn.model_selection import train_test_split
import pandas as pd
from sklearn.preprocessing import MinMaxScaler
from classifier import rand_balanced_dataset


def load_img_dataset():
    a = 5


def plot_results():
    a = 5


def save_features(features_tot):
    for idx, features in enumerate(features_tot):
        np.save("OutputProducts/features_classifier%d.npy" % idx, features)


def load_features(idx):
    features = np.load("OutputProducts/features_classifier%d.npy" % idx)
    return features


def select_dataset_with_features(selected_features, dataset):

    X = dataset.drop('Class', axis=1)
    y = dataset['Class']

    X = X[selected_features]

    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=0, stratify=y)

    return X_train, X_test, y_train, y_test


def load_aggregated_data(idx):
    data_non = pd.read_csv('OutputProducts/features_nondemented.csv')  # , nrows=20000)
    data_vmild = pd.read_csv('OutputProducts/features_vmild.csv')  # , nrows=20000)
    data_moderate = pd.read_csv('OutputProducts/features_moderate.csv')  # , nrows=20000)

    trans = MinMaxScaler()

    # perform a robust scaler transform of the dataset
    # TODO autoscale with STANDARDIZATION
    data_non = pd.DataFrame(trans.fit_transform(data_non)).rename(columns={512: 'Class'}).assign(Class=1)
    data_vmild = pd.DataFrame(trans.fit_transform(data_vmild)).rename(columns={512: 'Class'}).assign(Class=2)
    data_moderate = pd.DataFrame(trans.fit_transform(data_moderate)).rename(columns={512: 'Class'}).assign(Class=3)

    # aggregated_data = pd.concat([data_non, data_vmild, data_moderate], ignore_index=True)
    aggregated_data = rand_balanced_dataset(data_non, data_vmild, data_moderate, idx)
    return aggregated_data



