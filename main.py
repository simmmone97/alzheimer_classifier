from parser1 import parse
from cnn import cnn_feature_extraction
from data_handling import load_img_dataset, plot_results, select_dataset_with_features, load_aggregated_data
from classifier import feature_selection_3class, train_classifier, test_classifier
from data_handling import save_features, load_features

IMG_DATASET_FOLDER = "img_dataset_folder"
OUTPUT_PRODUCTS = "dataset_folder"

#parametri iniziali
opts_cnn = "None"
opts_classifier = "PLS-DA"


# options = parse()
print(opts_cnn, opts_classifier)

# access the dataset folder and load images as separated classes
load_img_dataset()

# extract features from the images and store them as separate csv file
dataset = cnn_feature_extraction(opts_cnn)

# perform feature selection using the AUC on previously extracted features
features_class1, features_class2, features_class3, dataset_aggregated = feature_selection_3class()

# given features after the feature selection has been performed, we obtain a smaller dataset
features = [features_class1, features_class2, features_class3]
save_features(features)

datasets = []

X_train, X_test, Y_train, Y_test = select_dataset_with_features(features_class2, dataset_aggregated)

# classifier train
train_classifier(opts_classifier, X_train, X_test, Y_train, Y_test)

# classifier test
# test_classifier(opts_classifier)

# plot results and store data (classifier, extracted features, results report)
# plot_results()

# UNCOMMENT FOR QUICK ACCESS CODE
# save_features(features)


def quick_run_iters():
    for j in range(3):
        for i in range(10):
            print("Run: %d %d" % (j, i))
            idx = j
            features_class2 = load_features(idx)
            dataset_aggregated = load_aggregated_data(idx)
            X_train, X_test, Y_train, Y_test = select_dataset_with_features(features_class2, dataset_aggregated)
            train_classifier(opts_classifier, X_train, X_test, Y_train, Y_test)
            print("")
